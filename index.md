# Shibari figures

## About this page

This website is a repository of shibari figures, with instruction sheets written in Markdown and drawings made with LaTeX + TikZ. It is yet under construction.

NB! Keep in mind that rope bondage is *dangerous* and should *never* be practiced without sufficient knowledge about rope safety and possible risks. This page is *not* intended to be a stand-alone course for rope beginners. It is meant purely as a collection of figures to learn, and assumes that these are practiced only with the necessary knowledge and experience in rope bondage. Also keep in mind that the rope figures listed here may contain errors. If you tie them, use your own common sense, and be aware of possible risks!

NB! Blue links are internal links to other pages on this website, and all materials on this site are SFW drawings. Green links are external links and may contain explicit images or videos, and are hence potentially *NSFW*.

The instruction sheets make frequent reference to knots listed in [The Ashley Book Of Knots](http://en.wikipedia.org/wiki/The_Ashley_Book_of_Knots) (marked with "ABOK" and the number of the knot). It is recommended as additional reference material.

## Figures

### Helpful knots and techniques

* [Extending rope: lark's head to square knot method](figures/knot/extsquare/)
* [Extending rope: sheet bend method](figures/knot/extsheet/)
* X friction
* L friction
* U friction

### Single column ties

* [Lark's head single column tie](figures/single/lark/)
* [Lark's head single column tie with extra wraps](figures/single/larkx/)
* [Somerville bowline](figures/single/somerville/)
* [Reef knot single column tie](figures/single/reef/)
* [Granny knot single column tie](figures/single/granny/)
* [Slipped overhand cuff](figures/single/slipover/)
* [Captured overhand cuff](figures/single/captover/)
* [Captured overhand gunslinger](figures/single/cogunslinger/)

### Double column ties

* [Lark's head double column tie](figures/double/lark/)
* [Somerville bowline double column](figures/double/somerville/)
* Figure 8 inline double column

### Arm ties

* [Basic armbinder](figures/arm/basic/)
* Extra strict armbinder
* Lace up armbinder
* [Dragonfly sleeve](figures/arm/dragonfly/)

### Leg ties

* Futomomo (simple)
* Futomomo (braided)
* Futomomo (X friction, for suspension)
* Agura (with waist rope)
* Agura (without waist rope)

### Chest harnesses

* [Pentagram harness](figures/chest/penta/)
* [Shinju](figures/chest/shinju/)
* [Bikini harness](figures/chest/bikini/)
* [Intimate chest harness](figures/chest/intimate/)
* [Quick shoulder harness](figures/chest/shoulder/)
* [Karada box tie](figures/chest/karadabox/)
* [Corselet harness](figures/chest/corselet/)
* [Under-ribs karada](figures/chest/urkarada/)

### Body harnesses

* [Star back harness](figures/body/starback/)

### Takate Kote

* [Basic TK with only lower kannukis](figures/tk/basic/)
* [2-Rope Gote TK](figures/tk/gote2tk/)

## Technical background

See the [GitLab project](https://gitlab.com/YpsilonOmega/shibari/) for source code, updates and the possibility to contribute.
