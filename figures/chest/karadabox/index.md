# Karada box tie

## Description

Despite its similarity with the classical box tie (takate kote), the karada box tie is listed under chest harnesses here. It should not be used for the same purposes as a box tie, but rather as a decorative chest tie. In particular, it is not suitable for suspension.

## Instructions

#. The tie starts with a [lark's head single colum tie](../../single/lark/) around the waist. Start with the center bight in the front and lay the ropes once around the body along R-Q.
#. After the first round, pull the ropes through the center bight and reverse direction to form a lark's head (ABOK #5).
#. Go around the waist in the opposite direction along S-T, just below the first strand R-Q.
#. In the front, pull the free ends through the double bight Q-S, then once around the strands towards Q and S and through the newly formed loop upwards.
#. Make two overhand knots (ABOK #46) into the free ends, leaving two distances of around 15cm between the knots and the waist tie. The result should be similar to this:

	![Karada box tie - front view.](front.png)

#. Split the ends and lay them separately over the shoulders A and B.
#. On the back, pull the free ends down underneath the strands R-Q and T-S.
#. Bring the arms in the box position and wrap the free ends twice around the forearms.
#. Secure the arm tie by making an overhand knot (ABOK #47) around the strands coming from A-B. This should create the lower part of the back figure:

	![Karada box tie - back view.](back.png)

#. Split the rope ends and go in different directions O and P around the lower part of the chest, under the arms.
#. In the front, coming from V and Z, pull the ends through the loop under the lower overhand knot, so that the loop opens in diamond shape.
#. Twist the ropes once on each side before going back towards U and Y, this time over the arms and to the back.
#. On the back, coming from M and N, tie a square knot (ABOK #1204) in such a way that the vertical strands coming from A and B pass through the knot under the strands to L and N and over those to K and M.
#. Continue to the front towards K and L.
#. On the left, pass the rope under the arm above the strand N-Y, then downward crossing over the strand towards Y, back again under the arm, next upward crossing over the strands towards L and N and finally under the arm towards the front X, as shown in the picture:

	![Karada box tie - left side.](left.png)

#. On the right, pass the rope under the arm above the strand M-U, then downward crossing over the strand towards U, back again under the arm, next upward crossing over the strands towards K and M and finally under the arm towards the front J, as shown in the picture:

	![Karada box tie - right side.](right.png)

#. In the front, coming from X and J, pull the ends through the loop between the two overhand knots, so that the loop opens in diamond shape.
#. Twist the ropes once on each side before going back towards W and I, with the twist being in opposite direction compared to the first loop U-V and Y-Z, then again over the arms and to the back.
#. On the back, coming from E and F, tie a square knot (ABOK #1204) in such a way that the vertical strands coming from A and B pass through the knot under the strands to D and F and over those to C and E.
#. Continue to the front towards C and D.
#. On the left, pass the rope under the arm above the strand F-W, then downward crossing over the strand towards W and back again under the arm towards H.
#. On the right, pass the rope under the arm above the strand E-I, then downward crossing over the strand towards I and back again under the arm towards G.
#. On the back, coming from G and F, tie a square knot (ABOK #1204) in such a way that the vertical strands coming from A and B pass through the knot under the strands to G and its free end and over those to H and its free end.
#. Another knot on top may help if the rope is too slippery for the previous knot to hold tight.

## References

* <http://www.theduchy.com/karada-box-tie/> - alternative finish with separately attached kannuki.
