# Quick shoulder harness

## Description

This harness is not particularly decorative or effective on its own. It is rather a quick and simple tie which creates an anchor point on the upper part of the chest, which can be used to connect other ties.

## Instructions

#. Start with the center bight on the back, centered between the shoulders.
#. Lay the rope ends forward under the right armpit C and back over the right shoulder A.
#. Cross the back of the neck, forward over the left shoulder B and back again under the left armpit D.
#. Pull the strand A-B down towards the center bight such that it forms a right angle, and align it with the center bight such that the ropes form an X along B-C and A-D.
#. Pull the long end of the rope through the center bight and underneath the strand A-B.
#. Lock the knot off with half-hitch (ABOK #48) across the strand to D.
#. Lay the rope ends towards the front under the right armpit E, under the strand C.
#. Go across the upper part of the chest, under the strand B-D, under the strand A-C and towards the left armpit F.
#. Lay the rope ends towards the back again, under the strand D.
#. Pull the rope ends under the strands going to C and E, and over the strand going to A.
#. Finish the knot with two half hitches over the strand going to B. The result should look like this:

	![Quick shoulder harness - front view.](front.png)

	![Quick shoulder harness - back view.](back.png)

## References

* <http://crash-restraint.com/ties/37>
