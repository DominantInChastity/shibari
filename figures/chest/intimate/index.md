# Intimate chest harness

## Description

In contrast to many other chest harnesses, for this harness the tying partner is located in front of his / her partner, hence the name "intimate".

## Instructions

#. Start with the center bight at the lower part of the chest and lay the rope around the back from A to B.
#. Pull the ends through the center bight, creating a lark's head (ABOK #5).
#. Lay the rope ends over the right shoulder C to the back, under the left arm D to the front, on the upper part of the chest to E and on the back to F.
#. On the upper part of the chest, under the strand D-E, tie a crossing knot (ABOK #1173) across the strand going to C, such that it crosses also over D-E.
#. On the back, lay the ropes from under the right arm G to the left shoulder H.
#. Pass the ends under the strands from D and F, through the center bight and finish with a half hitch over the strand going to B. The result should look like this:

	![Intimate chest harness - front view.](front.png)

	![Intimate chest harness - back view.](back.png)

## References

* <http://crash-restraint.com/ties/260>
