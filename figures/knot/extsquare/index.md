# Extending rope: lark's head to square knot method

## Description

The following method to extend rope is easy to use and memorize. It works best when the rope ends have knots and the loop formed cannot slip off, but if tied with some distance (5-10cm, depending on rope material) from the end, it works well also for whipped rope ends.

## Instructions

#. Take the center bight of the new rope, and fold it back across the remaining long ends A, forming a lark's head (ABOK #5):

	![First step: forming a loop](stage1.png)

#. Pull the two ends of the old rope (which should be of the same length) through the double loop from X to Y:

	![Second step: pushing the old rope through](stage2.png)

#. Fold the old rope backwards towards X and Y, away from the new rope A:

	![Third step: folding the old rope back](stage3.png)

#. Push the center bight from the new rope onto the old one and pull tight, forming a square knot (ABOK #1204):

	![Fourth step: pushing the center bight of the new rope onto the old one](stage4.png)

## References

* <http://www.theduchy.com/extending-rope/>
