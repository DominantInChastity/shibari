# Slipped overhand cuff

## Description

Most single column ties require to start tying from one end, or rather the center bight of a rope, while the running ends are still free. If the center bight is already tied to something else and one wants to tie a single column tie having only the running ends free, one can use this tie. Its use is therefore similar to the [captured overhand cuff](../captover/).

## Instructions

#. Tie a slipped overhand knot (slip knot, ABOK #529) such that the free rope ends (and not the fixed end Y) run through and form the adjustable loop.
#. Lay the working ends around the column from A to A.
#. Pull the rope ends through the loop and towards D.
#. Make another wrap around the column from D to D.
#. Pull the ends again through the loop, but this time in the opposite direction and towards C.
#. Make another wrap around the column from C to C.
#. Pull the ends again through the loop, this time again in the original direction and towards B.
#. Make another wrap around the column from B to B.
#. Pull the ends through the loop once more, once again in the opposite direction.
#. Secure the ends with a square knot (ABOK #1204) around the first wrap. The result should look like this:

	![Slipped overhand cuff - final position.](final.png)

## References

* <http://crash-restraint.com/blog/2010/04/28/the-slipped-overhand-cuff>
