# Captured overhand cuff

## Description

As with the [slipped overhand cuff](../slipover/), also the captured overhand cuff can be tied in the running ends of a rope, whose center bight is already used for another tie. It is in particular useful to tie a second limb, if the first limb is already tied using a different single column tie.

## Instructions

#. Take the running ends from the other limb A-B and make a single overhand knot (ABOK #1203) at the spot which will mark the edge of the cuff.
#. Go with the running ends once around the new limb which is to be tied.
#. Pull the ends through between the strands coming from A and B, on the far side of the overhand knot.
#. Make a second wrap around the limb.
#. Make a third wrap, ensuring that all three wraps have the same amount of slack.
#. With the running ends, cross upwards in front of all wraps, then down through the wraps, parallel to the limb to be tied.
#. Lock the tie off with a half hitch (ABOK #48) around the strands coming from A-B and pull tight.
#. The final result should have the following shape:

	![Captured overhand cuff - final position.](final.png)

## References

* <http://crash-restraint.com/ties/225>
