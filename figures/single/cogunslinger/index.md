# Captured overhand gunslinger

## Description

The captured overhand gunslinger uses a similar method as the [captured overhand cuff](../captover/) to create friction and stabilize the wraps of a single column tie. It is particularly useful to tie around the leg in a "gunslinger" style (hence the name) with a rope that comes from above, and continues downwards.

## Instructions

#. In the running ends coming from X, make a single overhand knot (ABOK #1202), such that the position of this knot will mark the lower edge of the tie.
#. Make a loop A around the leg, such that the rope bends by 90 degrees at the overhand knot.
#. Pass under the stem coming from X, right over the first overhand knot, then reverse direction and pass back over the stem.
#. Make another loop B around the leg, closely over the first one.
#. Coming from B, pass over the stem from X towards C.
#. Make a third loop C around the leg, closely over the second one.
#. Coming from C, pass under the stem from X, then downwards crossing over all three loops.
#. Run the free ends up again, now passing through under all three loops.
#. Lock the tie off with a half hitch (ABOK #48) around the stem towards X.
#. The final tie with three wraps around the leg should have the following shape:

	![Captured overhand gunslinger - three wrap version.](three.png)

#. Optionally, the wrap which crosses over the vertical stem may also be omitted, creating a two wrap version instead:

	![Captured overhand gunslinger - two wrap version.](two.png)

#. The free ends Y can now be used to continue downwards with another tie.

## References

* <http://crash-restraint.com/ties/225>
