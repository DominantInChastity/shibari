# Lark's head single column tie

## Description

The lark's head single column tie is a fast and simple single column tie. If more comfort is desired, a [version with extra wraps](../larkx/) may be used.

## Instructions

#. Start with the center bight and lay the rope once around the column from A to A.
#. Pull the ends through the center bight to form a lark's head (ABOK #5).
#. Reverse direction and lay the ropes once again around the column from B to B:

	![First lark's head and wraps around the column.](lark1.png)

#. Pull the ends through the double bight.
#. Form a "figure 4", laying the ends X over the wraps like this:

	![First step of the closing knot.](lark2.png)

#. Finish the knot with a half hitch (ABOK #48), by pulling the ends X underneath the wraps and through the "figure 4 loop". The result should look like this:

	![Finished lark's head single column tie.](final.png)

## References

* <http://crash-restraint.com/ties/19>
