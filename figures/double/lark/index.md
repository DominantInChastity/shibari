# Lark's head double column tie

## Description

The lark's head double column tie is a simple, though not the fastest double column tie.

## Instructions

#. Start with the center bight and lay the rope once around both columns.
#. Pull the ends through the center bight to form a lark's head (ABOK #5).
#. Reverse direction and lay the ropes once again around both columns.
#. Add a few extra wraps.
#. After the last wrap, go over all previous wraps and pull the ends through the double bight.
#. Keep the direction and lay the rope ends once fully around the wraps to tighten the cuff, then once more through the double bight.
#. Create a half hitch (ABOK #48) by reversing direction, thus forming a bight, going through all wraps, then over the wraps in the opposite direction and through the bight just formed. The result should look like this:

	![Lark's head double column tie (columns shown in gray)](final.png)

## References

* <http://crash-restraint.com/ties/25> (with 3 wraps)
* <http://crash-restraint.com/blog/2010/05/02/larks-head-based-double-column> (with 4 wraps)
