#!/bin/bash

for folder in `find figures -type d`
do
	mkdir -p .public/$folder
	cp $folder/*.png .public/$folder/ 2>/dev/null || :
	cp $folder/*.html .public/$folder/ 2>/dev/null || :
done

cp index.html shibari.css .public/
