\ProvidesClass{shibari}[2019/08/11 Shibari diagram]

\LoadClass{standalone}
\RequirePackage{helvet}
\RequirePackage{tikz}
\RequirePackage[utf8x]{inputenc}
\RequirePackage[T1]{fontenc}

\renewcommand{\familydefault}{\sfdefault}

\usetikzlibrary{knots,hobby}
\tikzset{knot diagram/every strand/.append style={ultra thick,black}}
\tikzset{every picture/.style={execute at end picture={\path ([shift={(-5pt,-5pt)}]current bounding box.south west) ([shift={(5pt,5pt)}]current bounding box.north east);}}}

\newcommand{\dstrand}{\strand[double=white, double distance=1.5mm]}
